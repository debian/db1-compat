From 2f49629dfdc096b6c947040d81a496330ed31467 Mon Sep 17 00:00:00 2001
From: Julian Gilbey <jdg@debian.org>
Date: Tue, 14 Jan 2014 02:08:55 +0000
Subject: Add db_dump185 utility

This provides dump compatibility with legacy database files.

Author: Colin Watson <cjwatson@debian.org>
Bug-Debian: http://bugs.debian.org/353637
Forwarded: no
Last-Update: 2024-03-13

Patch-Name: db_dump185.patch
---
 Makefile                |   7 +-
 db_dump185/db_dump185.1 | 956 ++++++++++++++++++++++++++++++++++++++++
 db_dump185/db_dump185.c | 356 +++++++++++++++
 3 files changed, 1318 insertions(+), 1 deletion(-)
 create mode 100644 db_dump185/db_dump185.1
 create mode 100644 db_dump185/db_dump185.c

diff --git a/Makefile b/Makefile
index ae4e1b5..387d72d 100644
--- a/Makefile
+++ b/Makefile
@@ -24,7 +24,7 @@ CFLAGS ?= -O2
 XCPPFLAGS = -I. -D_REENTRANT -D__DBINTERFACE_PRIVATE -DUSE_LIBDB1 -D_GNU_SOURCE
 libdb.so-version=.2
 
-all: libdb1.so$(libdb.so-version)
+all: libdb1.so$(libdb.so-version) db_dump185/db_dump185
 
 %.o: %.c
 	$(CC) $(XCPPFLAGS) $(CPPFLAGS) $(CFLAGS) -c -g -o $@ $<
@@ -36,7 +36,12 @@ libdb1.so$(libdb.so-version): $(patsubst %,%.os,$(libdb1-routines))
 	      -Wl,-soname=libdb.so$(libdb.so-version) $(LDFLAGS) \
 	      -shared -o $@ $^ -lc
 
+db_dump185/db_dump185: db_dump185/db_dump185.c libdb1.so$(libdb.so-version)
+	$(CC) -I. $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) \
+	      -o $@ $< libdb1.so$(libdb.so-version)
+
 clean:
 	rm -f $(patsubst %,%.o,$(libdb1-routines))
 	rm -f $(patsubst %,%.os,$(libdb1-routines))
 	rm -f libdb1.so$(libdb.so-version) *~
+	rm -f db_dump185/db_dump185
diff --git a/db_dump185/db_dump185.1 b/db_dump185/db_dump185.1
new file mode 100644
index 0000000..9fb305d
--- /dev/null
+++ b/db_dump185/db_dump185.1
@@ -0,0 +1,956 @@
+.ds TYPE C
+.\"
+.\" See the file LICENSE for redistribution information.
+.\"
+.\" Copyright (c) 1996, 1997, 1998
+.\"	Sleepycat Software.  All rights reserved.
+.\"
+.\"	@(#)db_dump.so	10.10 (Sleepycat) 4/10/98
+.\"
+.\"
+.\" See the file LICENSE for redistribution information.
+.\"
+.\" Copyright (c) 1997, 1998
+.\"	Sleepycat Software.  All rights reserved.
+.\"
+.\"	@(#)macros.so	10.45 (Sleepycat) 5/4/98
+.\"
+.\" This manpage was originally for db_dump from a later version of
+.\" the DB, so many of these initial macros are now defunct. (jdg, 2/06)
+.\"
+.\" We don't want hyphenation for any HTML documents.
+.if !d HTML .ds HTML
+.ie '\*[HTML]'YES'\{\
+.de Hy
+..
+.de Nh
+..
+.nh
+\}
+.el\{\
+.de Hy
+.hy
+..
+.de Nh
+.nh
+..
+\}
+.\" The alternative text macro
+.\" This macro takes two arguments:
+.\"	+ the text produced if this is a "C" manpage
+.\"	+ the text produced if this is a "CXX" or "JAVA" manpage
+.\"
+.de Al
+.ie '\*[TYPE]'C'\{\\$1
+\}
+.el\{\\$2
+\}
+..
+.\" Scoped name macro.
+.\" Produces a_b, a::b, a.b depending on language
+.\" This macro takes two arguments:
+.\"	+ the class or prefix (without underscore)
+.\"	+ the name within the class or following the prefix
+.de Sc
+.ie '\*[TYPE]'C'\{\\$1_\\$2
+\}
+.el\{\
+.ie '\*[TYPE]'CXX'\{\\$1::\\$2
+\}
+.el\{\\$1.\\$2
+\}
+\}
+..
+.\" Scoped name for Java.
+.\" Produces Db.b, for Java, otherwise just b.  This macro is used for
+.\" constants that must be scoped in Java, but are global otherwise.
+.\" This macro takes two arguments:
+.\"	+ the class
+.\"	+ the name within the class or following the prefix
+.de Sj
+.ie '\*[TYPE]'JAVA'\{\
+.TP 5
+Db.\\$1\}
+.el\{\
+.TP 5
+\\$1\}
+..
+.\" The general information text macro.
+.de Gn
+.ie '\*[TYPE]'C'\{The DB library is a family of groups of functions that provides a modular
+programming interface to transactions and record-oriented file access.
+The library includes support for transactions, locking, logging and file
+page caching, as well as various indexed access methods.
+Many of the functional groups (e.g., the file page caching functions)
+are useful independent of the other DB functions,
+although some functional groups are explicitly based on other functional
+groups (e.g., transactions and logging).
+\}
+.el\{The DB library is a family of classes that provides a modular
+programming interface to transactions and record-oriented file access.
+The library includes support for transactions, locking, logging and file
+page caching, as well as various indexed access methods.
+Many of the classes (e.g., the file page caching class)
+are useful independent of the other DB classes,
+although some classes are explicitly based on other classes
+(e.g., transactions and logging).
+\}
+..
+.\" The library error macro, the local error macro.
+.\" These macros take one argument:
+.\"	+ the function name.
+.de Ee
+The
+.I \\$1
+.ie '\*[TYPE]'C'\{function may fail and return
+.I errno
+\}
+.el\{method may fail and throw a
+.IR DbException (3)
+.if '\*[TYPE]'CXX'\{
+or return
+.I errno
+\}
+\}
+for any of the errors specified for the following DB and library functions:
+..
+.de Ec
+In addition, the
+.I \\$1
+.ie '\*[TYPE]'C'\{function may fail and return
+.I errno
+\}
+.el\{method may fail and throw a
+.IR DbException (3)
+.ie '\*[TYPE]'CXX'\{or return
+.I errno
+\}
+.el\{encapsulating an
+.I errno
+\}
+\}
+for the following conditions:
+..
+.de Ea
+[EAGAIN]
+A lock was unavailable.
+..
+.de Eb
+[EBUSY]
+The shared memory region was in use and the force flag was not set.
+..
+.de Em
+[EAGAIN]
+The shared memory region was locked and (repeatedly) unavailable.
+..
+.de Ei
+[EINVAL]
+An invalid flag value or parameter was specified.
+..
+.de Es
+[EACCES]
+An attempt was made to modify a read-only database.
+..
+.de Et
+The DB_THREAD flag was specified and spinlocks are not implemented for
+this architecture.
+..
+.de Ep
+[EPERM]
+Database corruption was detected.
+All subsequent database calls (other than
+.ie '\*[TYPE]'C'\{\
+.IR DB->close )
+\}
+.el\{\
+.IR Db::close )
+\}
+will return EPERM.
+..
+.de Ek
+.if '\*[TYPE]'CXX'\{\
+Methods marked as returning
+.I errno
+will, by default, throw an exception that encapsulates the error information.
+The default error behavior can be changed, see
+.IR DbException (3).
+\}
+..
+.\" The SEE ALSO text macro
+.de Sa
+.nh
+.na
+.IR db_archive (1),
+.IR db_checkpoint (1),
+.IR db_deadlock (1),
+.IR db_dump (1),
+.IR db_load (1),
+.IR db_recover (1),
+.IR db_stat (1).
+(Note that on Debian systems, some of these manpages and programs have
+been renamed to things like
+.B db4.3_stat
+to distinguish between the multiple DB versions.)
+.ad
+.Hy
+..
+.\" The function header macro.
+.\" This macro takes one argument:
+.\"	+ the function name.
+.de Fn
+.in 2
+.I \\$1
+.in
+..
+.\" The XXX_open function text macro, for merged create/open calls.
+.\" This macro takes two arguments:
+.\"	+ the interface, e.g., "transaction region"
+.\"	+ the prefix, e.g., "txn" (or the class name for C++, e.g., "DbTxn")
+.de Co
+.ie '\*[TYPE]'C'\{\
+.Fn \\$2_open
+The
+.I \\$2_open
+function copies a pointer, to the \\$1 identified by the
+.B directory
+.IR dir ,
+into the memory location referenced by
+.IR regionp .
+.PP
+If the
+.I dbenv
+argument to
+.I \\$2_open
+was initialized using
+.IR db_appinit ,
+.I dir
+is interpreted as described by
+.IR db_appinit (3).
+\}
+.el\{\
+.Fn \\$2::open
+The
+.I \\$2::open
+.ie '\*[TYPE]'CXX'\{\
+method copies a pointer, to the \\$1 identified by the
+.B directory
+.IR dir ,
+into the memory location referenced by
+.IR regionp .
+\}
+.el\{\
+method returns a \\$1 identified by the
+.B directory
+.IR dir .
+\}
+.PP
+If the
+.I dbenv
+argument to
+.I \\$2::open
+was initialized using
+.IR DbEnv::appinit ,
+.I dir
+is interpreted as described by
+.IR DbEnv (3).
+\}
+.PP
+Otherwise,
+if
+.I dir
+is not NULL,
+it is interpreted relative to the current working directory of the process.
+If
+.I dir
+is NULL,
+the following environment variables are checked in order:
+``TMPDIR'', ``TEMP'', and ``TMP''.
+If one of them is set,
+\\$1 files are created relative to the directory it specifies.
+If none of them are set, the first possible one of the following
+directories is used:
+.IR /var/tmp ,
+.IR /usr/tmp ,
+.IR /temp ,
+.IR /tmp ,
+.I C:/temp
+and
+.IR C:/tmp .
+.PP
+All files associated with the \\$1 are created in this directory.
+This directory must already exist when
+.ie '\*[TYPE]'C'\{
+\\$1_open
+\}
+.el\{\
+\\$2::open
+\}
+is called.
+If the \\$1 already exists,
+the process must have permission to read and write the existing files.
+If the \\$1 does not already exist,
+it is optionally created and initialized.
+..
+.\" The common close language macro, for discarding created regions
+.\" This macro takes one argument:
+.\"	+ the function prefix, e.g., txn (the class name for C++, e.g., DbTxn)
+.de Cc
+In addition, if the
+.I dir
+argument to
+.ie '\*[TYPE]'C'\{\
+.ds Va db_appinit
+.ds Vo \\$1_open
+.ds Vu \\$1_unlink
+\}
+.el\{\
+.ds Va DbEnv::appinit
+.ds Vo \\$1::open
+.ds Vu \\$1::unlink
+\}
+.I \\*(Vo
+was NULL
+and
+.I dbenv
+was not initialized using
+.IR \\*(Va ,
+.if '\\$1'memp'\{\
+or the DB_MPOOL_PRIVATE flag was set,
+\}
+all files created for this shared region will be removed,
+as if
+.I \\*(Vu
+were called.
+.rm Va
+.rm Vo
+.rm Vu
+..
+.\" The DB_ENV information macro.
+.\" This macro takes two arguments:
+.\"	+ the function called to open, e.g., "txn_open"
+.\"	+ the function called to close, e.g., "txn_close"
+.de En
+.ie '\*[TYPE]'C'\{\
+based on the
+.I dbenv
+argument to
+.IR \\$1 ,
+which is a pointer to a structure of type DB_ENV (typedef'd in <db.h>).
+Applications will normally use the same DB_ENV structure (initialized
+by
+.IR db_appinit (3)),
+as an argument to all of the subsystems in the DB package.
+.PP
+References to the DB_ENV structure are maintained by DB,
+so it may not be discarded until the last close function,
+corresponding to an open function for which it was an argument,
+has returned.
+In order to ensure compatibility with future releases of DB, all fields of
+the DB_ENV structure that are not explicitly set should be initialized to 0
+before the first time the structure is used.
+Do this by declaring the structure external or static, or by calling the C
+library routine
+.IR bzero (3)
+or
+.IR memset (3).
+.PP
+The fields of the DB_ENV structure used by
+.I \\$1
+are described below.
+.if '\*[TYPE]'CXX'\{\
+As references to the DB_ENV structure may be maintained by
+.IR \\$1 ,
+it is necessary that the DB_ENV structure and memory it references be valid
+until the
+.I \\$2
+function is called.
+\}
+.ie '\\$1'db_appinit'\{The
+.I dbenv
+argument may not be NULL.
+If any of the fields of the
+.I dbenv
+are set to 0,
+defaults appropriate for the system are used where possible.
+\}
+.el\{If
+.I dbenv
+is NULL
+or any of its fields are set to 0,
+defaults appropriate for the system are used where possible.
+\}
+.PP
+The following fields in the DB_ENV structure may be initialized before calling
+.IR \\$1 :
+\}
+.el\{\
+based on which set methods have been used.
+It is expected that applications will use a single DbEnv object as the
+argument to all of the subsystems in the DB package.
+The fields of the DbEnv object used by
+.I \\$1
+are described below.
+As references to the DbEnv object may be maintained by
+.IR \\$1 ,
+it is necessary that the DbEnv object and memory it references be valid
+until the object is destroyed.
+.ie '\\$1'appinit'\{\
+The
+.I dbenv
+argument may not be NULL.
+If any of the fields of the
+.I dbenv
+are set to 0,
+defaults appropriate for the system are used where possible.
+\}
+.el\{\
+Any of the DbEnv fields that are not explicitly set will default to
+appropriate values.
+\}
+.PP
+The following fields in the DbEnv object may be initialized, using the
+appropriate set method, before calling
+.IR \\$1 :
+\}
+..
+.\" The DB_ENV common fields macros.
+.de Se
+.if '\*[TYPE]'JAVA'\{\
+.TP 5
+DbErrcall db_errcall;
+.ns
+.TP 5
+String db_errpfx;
+.ns
+.TP 5
+int db_verbose;
+The error fields of the DbEnv behave as described for
+.IR DbEnv (3).
+\}
+.ie '\*[TYPE]'CXX'\{\
+.TP 5
+void *(*db_errcall)(char *db_errpfx, char *buffer);
+.ns
+.TP 5
+FILE *db_errfile;
+.ns
+.TP 5
+const char *db_errpfx;
+.ns
+.TP 5
+class ostream *db_error_stream;
+.ns
+.TP 5
+int db_verbose;
+The error fields of the DbEnv behave as described for
+.IR DbEnv (3).
+\}
+.el\{\
+void *(*db_errcall)(char *db_errpfx, char *buffer);
+.ns
+.TP 5
+FILE *db_errfile;
+.ns
+.TP 5
+const char *db_errpfx;
+.ns
+.TP 5
+int db_verbose;
+The error fields of the DB_ENV behave as described for
+.IR db_appinit (3).
+.sp
+\}
+..
+.\" The open flags.
+.de Fm
+The
+.I flags
+and
+.I mode
+arguments specify how files will be opened and/or created when they
+don't already exist.
+The flags value is specified by
+.BR or 'ing
+together one or more of the following values:
+.Sj DB_CREATE
+Create any underlying files, as necessary.
+If the files do not already exist and the DB_CREATE flag is not specified,
+the call will fail.
+..
+.\" DB_THREAD open flag macro.
+.\" This macro takes two arguments:
+.\"	+ the open function name
+.\"	+ the object it returns.
+.de Ft
+.TP 5
+.Sj DB_THREAD
+Cause the \\$2 handle returned by the
+.I \\$1
+.Al function method
+to be useable by multiple threads within a single address space,
+i.e., to be ``free-threaded''.
+.if '\*[TYPE]'JAVA'\{\
+Threading is assumed in the Java API,
+so no special flags are required,
+and DB functions will always behave as if the DB_THREAD flag was specified.
+\}
+..
+.\" The mode macro.
+.\" This macro takes one argument:
+.\"	+ the subsystem name.
+.de Mo
+All files created by the \\$1 are created with mode
+.I mode
+(as described in
+.IR chmod (2))
+and modified by the process' umask value at the time of creation (see
+.IR umask (2)).
+The group ownership of created files is based on the system and directory
+defaults, and is not further specified by DB.
+..
+.\" The application exits macro.
+.\" This macro takes one argument:
+.\"	+ the application name.
+.de Ex
+The
+.I \\$1
+utility exits 0 on success, and >0 if an error occurs.
+..
+.\" The application -h section.
+.\" This macro takes one argument:
+.\"	+ the application name
+.de Dh
+DB_HOME
+If the
+.B \-h
+option is not specified and the environment variable
+.I DB_HOME
+is set, it is used as the path of the database home, as described in
+.IR db_appinit (3).
+..
+.\" The function DB_HOME ENVIRONMENT VARIABLES section.
+.\" This macro takes one argument:
+.\"	+ the open function name
+.de Eh
+DB_HOME
+If the
+.I dbenv
+argument to
+.I \\$1
+was initialized using
+.IR db_appinit ,
+the environment variable DB_HOME may be used as the path of the database
+home for the interpretation of the
+.I dir
+argument to
+.IR \\$1 ,
+as described in
+.IR db_appinit (3).
+.if \\n(.$>1 \{Specifically,
+.I \\$1
+is affected by the configuration string value of \\$2.\}
+..
+.\" The function TMPDIR ENVIRONMENT VARIABLES section.
+.\" This macro takes two arguments:
+.\"	+ the interface, e.g., "transaction region"
+.\"	+ the prefix, e.g., "txn" (or the class name for C++, e.g., "DbTxn")
+.de Ev
+TMPDIR
+If the
+.I dbenv
+argument to
+.ie '\*[TYPE]'C'\{\
+.ds Vo \\$2_open
+\}
+.el\{\
+.ds Vo \\$2::open
+\}
+.I \\*(Vo
+was NULL or not initialized using
+.IR db_appinit ,
+the environment variable TMPDIR may be used as the directory in which to
+create the \\$1,
+as described in the
+.I \\*(Vo
+section above.
+.rm Vo
+..
+.\" The unused flags macro.
+.de Fl
+The
+.I flags
+parameter is currently unused, and must be set to 0.
+..
+.\" The no-space TP macro.
+.de Nt
+.br
+.ns
+.TP 5
+..
+.\" The return values of the functions macros.
+.\" Rc is the standard two-value return with a suffix for more values.
+.\" Ro is the standard two-value return but there were previous values.
+.\" Rt is the standard two-value return, returning errno, 0, or < 0.
+.\" These macros take one argument:
+.\"	+ the routine name
+.de Rc
+The
+.I \\$1
+.ie '\*[TYPE]'C'\{function returns the value of
+.I errno
+on failure,
+0 on success,
+\}
+.el\{method throws a
+.IR DbException (3)
+.ie '\*[TYPE]'CXX'\{or returns the value of
+.I errno
+on failure,
+0 on success,
+\}
+.el\{that encapsulates an
+.I errno
+on failure,
+\}
+\}
+..
+.de Ro
+Otherwise, the
+.I \\$1
+.ie '\*[TYPE]'C'\{function returns the value of
+.I errno
+on failure and 0 on success.
+\}
+.el\{method throws a
+.IR DbException (3)
+.ie '\*[TYPE]'CXX'\{or returns the value of
+.I errno
+on failure and 0 on success.
+\}
+.el\{that encapsulates an
+.I errno
+on failure,
+\}
+\}
+..
+.de Rt
+The
+.I \\$1
+.ie '\*[TYPE]'C'\{function returns the value of
+.I errno
+on failure and 0 on success.
+\}
+.el\{method throws a
+.IR DbException (3)
+.ie '\*[TYPE]'CXX'\{or returns the value of
+.I errno
+on failure and 0 on success.
+\}
+.el\{that encapsulates an
+.I errno
+on failure.
+\}
+\}
+..
+.\" The TXN id macro.
+.de Tx
+.IP
+If the file is being accessed under transaction protection,
+the
+.I txnid
+parameter is a transaction ID returned from
+.IR txn_begin ,
+otherwise, NULL.
+..
+.\" The XXX_unlink function text macro.
+.\" This macro takes two arguments:
+.\"	+ the interface, e.g., "transaction region"
+.\"	+ the prefix (for C++, this is the class name)
+.de Un
+.ie '\*[TYPE]'C'\{\
+.ds Va db_appinit
+.ds Vc \\$2_close
+.ds Vo \\$2_open
+.ds Vu \\$2_unlink
+\}
+.el\{\
+.ds Va DbEnv::appinit
+.ds Vc \\$2::close
+.ds Vo \\$2::open
+.ds Vu \\$2::unlink
+\}
+.Fn \\*(Vu
+The
+.I \\*(Vu
+.Al function method
+destroys the \\$1 identified by the directory
+.IR dir ,
+removing all files used to implement the \\$1.
+.ie '\\$2'log' \{(The log files themselves and the directory
+.I dir
+are not removed.)\}
+.el \{(The directory
+.I dir
+is not removed.)\}
+If there are processes that have called
+.I \\*(Vo
+without calling
+.I \\*(Vc
+(i.e., there are processes currently using the \\$1),
+.I \\*(Vu
+will fail without further action,
+unless the force flag is set,
+in which case
+.I \\*(Vu
+will attempt to remove the \\$1 files regardless of any processes
+still using the \\$1.
+.PP
+The result of attempting to forcibly destroy the region when a process
+has the region open is unspecified.
+Processes using a shared memory region maintain an open file descriptor
+for it.
+On UNIX systems, the region removal should succeed
+and processes that have already joined the region should continue to
+run in the region without change,
+however processes attempting to join the \\$1 will either fail or
+attempt to create a new region.
+On other systems, e.g., WNT, where the
+.IR unlink (2)
+system call will fail if any process has an open file descriptor
+for the file,
+the region removal will fail.
+.PP
+In the case of catastrophic or system failure,
+database recovery must be performed (see
+.IR db_recover (1)
+or the DB_RECOVER and DB_RECOVER_FATAL flags to
+.IR \\*(Va (3)).
+Alternatively, if recovery is not required because no database state is
+maintained across failures,
+it is possible to clean up a \\$1 by removing all of the
+files in the directory specified to the
+.I \\*(Vo
+.Al function, method,
+as \\$1 files are never created in any directory other than the one
+specified to
+.IR \\*(Vo .
+Note, however,
+that this has the potential to remove files created by the other DB
+subsystems in this database environment.
+.PP
+.Rt \\*(Vu
+.rm Va
+.rm Vo
+.rm Vu
+.rm Vc
+..
+.\" Signal paragraph for standard utilities.
+.\" This macro takes one argument:
+.\"	+ the utility name.
+.de Si
+The
+.I \\$1
+utility attaches to DB shared memory regions.
+In order to avoid region corruption,
+it should always be given the chance to detach and exit gracefully.
+To cause
+.I \\$1
+to clean up after itself and exit,
+send it an interrupt signal (SIGINT).
+..
+.\" Logging paragraph for standard utilities.
+.\" This macro takes one argument:
+.\"	+ the utility name.
+.de Pi
+.B \-L
+Log the execution of the \\$1 utility to the specified file in the
+following format, where ``###'' is the process ID, and the date is
+the time the utility starting running.
+.sp
+\\$1: ### Wed Jun 15 01:23:45 EDT 1995
+.sp
+This file will be removed if the \\$1 utility exits gracefully.
+..
+.\" Malloc paragraph.
+.\" This macro takes one argument:
+.\"	+ the allocated object
+.de Ma
+.if !'\*[TYPE]'JAVA'\{\
+\\$1 are created in allocated memory.
+If
+.I db_malloc
+is non-NULL,
+it is called to allocate the memory,
+otherwise,
+the library function
+.IR malloc (3)
+is used.
+The function
+.I db_malloc
+must match the calling conventions of the
+.IR malloc (3)
+library routine.
+Regardless,
+the caller is responsible for deallocating the returned memory.
+To deallocate the returned memory,
+free each returned memory pointer;
+pointers inside the memory do not need to be individually freed.
+\}
+..
+.\" Underlying function paragraph.
+.\" This macro takes two arguments:
+.\"	+ the function name
+.\"	+ the utility name
+.de Uf
+The
+.I \\$1
+.Al function method
+is the underlying function used by the
+.IR \\$2 (1)
+utility.
+See the source code for the
+.I \\$2
+utility for an example of using
+.I \\$1
+in a UNIX environment.
+..
+.\" Underlying function paragraph, for C++.
+.\" This macro takes three arguments:
+.\"	+ the C++ method name
+.\"	+ the function name for C
+.\"	+ the utility name
+.de Ux
+The
+.I \\$1
+method is based on the C
+.I \\$2
+function, which
+is the underlying function used by the
+.IR \\$3 (1)
+utility.
+See the source code for the
+.I \\$3
+utility for an example of using
+.I \\$2
+in a UNIX environment.
+..
+.TH DB_DUMP185 1 "April 10, 1998"
+.UC 7
+.SH NAME
+db_dump185 \- dump DB 1.85 legacy database files
+.SH SYNOPSIS
+\fBdb_dump185\fP [\fB-p\fP] [\fB-f output\fP] db_file
+.SH DESCRIPTION
+The
+.I db_dump185
+utility reads the version 1.85 database file
+.I db_file
+and writes it to the standard output using a portable flat-text format
+understood by the
+.IR db_load (1)
+utility.
+The argument
+.I db_file
+must be a file produced using the version 1.85
+.I DB
+library functions.
+.PP
+The options are as follows:
+.TP 5
+.B \-f
+Write to the specified
+.I file
+instead of to the standard output.
+.TP 5
+.B \-p
+If characters in either the key or data items are printing characters
+(as defined by
+.IR isprint (3)),
+use printing characters in
+.I file
+to represent them.
+This option permits users to use standard text editors to modify
+the contents of databases.
+.IP
+Note, different systems may have different notions as to what characters
+are ``printing'', and databases dumped in this manner may be less portable
+to external systems.
+.PP
+Dumping and reloading hash databases that use user-defined hash functions
+will result in new databases that use the default hash function.
+While using the default hash function may not be optimal for the new database,
+it will continue to work correctly.
+.PP
+Dumping and reloading btree databases that use user-defined prefix or
+comparison functions will result in new databases that use the default
+prefix and comparison functions.
+.ft B
+In this case, it is quite likely that the database will be damaged
+beyond repair permitting neither record storage or retrieval.
+.ft R
+.PP
+The only available workaround for either case is to modify the sources
+for the
+.IR db_load (1)
+utility to load the database using the correct hash, prefix and comparison
+functions.
+.PP
+.Ex db_dump
+.SH "OUTPUT FORMATS
+There are two output formats used by
+.IR db_dump .
+.PP
+In both output formats, the first few lines of the output contain
+header information describing the underlying access method, filesystem page
+size and other bookkeeping information.
+This information is output in ``name=value'' pairs, where ``name'' may
+be any of the keywords listed in the
+.IR db_load (1)
+manual page, and ``value'' will be its value.
+While this header information can be edited before the database is
+reloaded,
+there is rarely any reason to do so, as all of this information can be
+overridden by command-line arguments to
+.IR db_load .
+.PP
+Following the header information are the key/data pairs from the database.
+If the database being dumped is of type
+.B btree
+or
+.BR hash ,
+the output will be paired lines of text,
+where the first line of the pair is the key item,
+and the second line of the pair is its corresponding data item.
+If the database being dumped is of type
+.BR recno ,
+the output will be lines of text,
+where each line is a new data item for the database.
+.PP
+If the
+.B \-p
+option was specified, each output line will consist of single characters
+representing any characters from the database that were ``printing'',
+and backslash (``\e'') escaped characters for any that were not.
+Backslash characters appearing in the output mean one of two things:
+if the backslash character precedes another backslash character,
+it means that a literal backslash character occurred in the key or data
+item.
+If the backslash character precedes any other character,
+the next two characters should be interpreted as hexadecimal specification
+of a single character, e.g., ``\e0a'' is a newline character in the ASCII
+character set.
+.PP
+If the
+.B \-p
+option was not specified, each output line will consist of paired hexadecimal
+values, e.g., the line ``726f6f74'' is the string ``root'' in the ASCII
+character set.
+.PP
+In both output formats,
+a single newline character ends both the key and data items.
+.SH "SEE ALSO"
+.IR isprint (3)
+.PP
+.Gn
+.PP
+.Sa
diff --git a/db_dump185/db_dump185.c b/db_dump185/db_dump185.c
new file mode 100644
index 0000000..5e82fc4
--- /dev/null
+++ b/db_dump185/db_dump185.c
@@ -0,0 +1,356 @@
+/*-
+ * See the file LICENSE for redistribution information.
+ *
+ * Copyright (c) 1996-2004
+ *	Sleepycat Software.  All rights reserved.
+ *
+ * $Id: db_dump185.c,v 11.19 2004/01/28 03:36:00 bostic Exp $
+ */
+
+#ifndef lint
+static char copyright[] =
+    "Copyright (c) 1996-2004\nSleepycat Software Inc.  All rights reserved.\n";
+#endif
+
+#include <sys/types.h>
+
+#include <ctype.h>
+#include <errno.h>
+#include <fcntl.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <string.h>
+#include <unistd.h>
+
+#include <db.h>
+
+/* Hash Table Information */
+typedef struct hashhdr185 {		/* Disk resident portion */
+	int		magic;		/* Magic NO for hash tables */
+	int		version;	/* Version ID */
+	u_int32_t	lorder;		/* Byte Order */
+	int		bsize;		/* Bucket/Page Size */
+	int		bshift;		/* Bucket shift */
+	int		dsize;		/* Directory Size */
+	int		ssize;		/* Segment Size */
+	int		sshift;		/* Segment shift */
+	int		ovfl_point;	/* Where overflow pages are being
+					 * allocated */
+	int		last_freed;	/* Last overflow page freed */
+	int		max_bucket;	/* ID of Maximum bucket in use */
+	int		high_mask;	/* Mask to modulo into entire table */
+	int		low_mask;	/* Mask to modulo into lower half of
+					 * table */
+	int		ffactor;	/* Fill factor */
+	int		nkeys;		/* Number of keys in hash table */
+} HASHHDR185;
+typedef struct htab185	 {		/* Memory resident data structure */
+	HASHHDR185	hdr;		/* Header */
+} HTAB185;
+
+/* Hash Table Information */
+typedef struct hashhdr186 {	/* Disk resident portion */
+	int32_t	magic;		/* Magic NO for hash tables */
+	int32_t	version;	/* Version ID */
+	int32_t	lorder;		/* Byte Order */
+	int32_t	bsize;		/* Bucket/Page Size */
+	int32_t	bshift;		/* Bucket shift */
+	int32_t	ovfl_point;	/* Where overflow pages are being allocated */
+	int32_t	last_freed;	/* Last overflow page freed */
+	int32_t	max_bucket;	/* ID of Maximum bucket in use */
+	int32_t	high_mask;	/* Mask to modulo into entire table */
+	int32_t	low_mask;	/* Mask to modulo into lower half of table */
+	int32_t	ffactor;	/* Fill factor */
+	int32_t	nkeys;		/* Number of keys in hash table */
+	int32_t	hdrpages;	/* Size of table header */
+	int32_t	h_charkey;	/* value of hash(CHARKEY) */
+#define	NCACHED	32		/* number of bit maps and spare points */
+	int32_t	spares[NCACHED];/* spare pages for overflow */
+				/* address of overflow page bitmaps */
+	u_int16_t bitmaps[NCACHED];
+} HASHHDR186;
+typedef struct htab186	 {		/* Memory resident data structure */
+	void *unused[2];
+	HASHHDR186	hdr;		/* Header */
+} HTAB186;
+
+typedef struct _epgno {
+	u_int32_t pgno;			/* the page number */
+	u_int16_t index;		/* the index on the page */
+} EPGNO;
+
+typedef struct _epg {
+	void	*page;			/* the (pinned) page */
+	u_int16_t index;		/* the index on the page */
+} EPG;
+
+typedef struct _cursor {
+	EPGNO	 pg;			/* B: Saved tree reference. */
+	DBT	 key;			/* B: Saved key, or key.data == NULL. */
+	u_int32_t rcursor;		/* R: recno cursor (1-based) */
+
+#define	CURS_ACQUIRE	0x01		/*  B: Cursor needs to be reacquired. */
+#define	CURS_AFTER	0x02		/*  B: Unreturned cursor after key. */
+#define	CURS_BEFORE	0x04		/*  B: Unreturned cursor before key. */
+#define	CURS_INIT	0x08		/* RB: Cursor initialized. */
+	u_int8_t flags;
+} CURSOR;
+
+/* The in-memory btree/recno data structure. */
+typedef struct _btree {
+	void	 *bt_mp;		/* memory pool cookie */
+
+	void	 *bt_dbp;		/* pointer to enclosing DB */
+
+	EPG	  bt_cur;		/* current (pinned) page */
+	void	 *bt_pinned;		/* page pinned across calls */
+
+	CURSOR	  bt_cursor;		/* cursor */
+
+	EPGNO	  bt_stack[50];		/* stack of parent pages */
+	EPGNO	 *bt_sp;		/* current stack pointer */
+
+	DBT	  bt_rkey;		/* returned key */
+	DBT	  bt_rdata;		/* returned data */
+
+	int	  bt_fd;		/* tree file descriptor */
+
+	u_int32_t bt_free;		/* next free page */
+	u_int32_t bt_psize;		/* page size */
+	u_int16_t bt_ovflsize;		/* cut-off for key/data overflow */
+	int	  bt_lorder;		/* byte order */
+					/* sorted order */
+	enum { NOT, BACK, FORWARD } bt_order;
+	EPGNO	  bt_last;		/* last insert */
+
+					/* B: key comparison function */
+	int	(*bt_cmp) __P((DBT *, DBT *));
+					/* B: prefix comparison function */
+	size_t	(*bt_pfx) __P((DBT *, DBT *));
+					/* R: recno input function */
+	int	(*bt_irec) __P((struct _btree *, u_int32_t));
+
+	FILE	 *bt_rfp;		/* R: record FILE pointer */
+	int	  bt_rfd;		/* R: record file descriptor */
+
+	void	 *bt_cmap;		/* R: current point in mapped space */
+	void	 *bt_smap;		/* R: start of mapped space */
+	void	 *bt_emap;		/* R: end of mapped space */
+	size_t	  bt_msize;		/* R: size of mapped region. */
+
+	u_int32_t bt_nrecs;		/* R: number of records */
+	size_t	  bt_reclen;		/* R: fixed record length */
+	u_char	  bt_bval;		/* R: delimiting byte/pad character */
+
+/*
+ * NB:
+ * B_NODUPS and R_RECNO are stored on disk, and may not be changed.
+ */
+#define	B_INMEM		0x00001		/* in-memory tree */
+#define	B_METADIRTY	0x00002		/* need to write metadata */
+#define	B_MODIFIED	0x00004		/* tree modified */
+#define	B_NEEDSWAP	0x00008		/* if byte order requires swapping */
+#define	B_RDONLY	0x00010		/* read-only tree */
+
+#define	B_NODUPS	0x00020		/* no duplicate keys permitted */
+#define	R_RECNO		0x00080		/* record oriented tree */
+
+#define	R_CLOSEFP	0x00040		/* opened a file pointer */
+#define	R_EOF		0x00100		/* end of input file reached. */
+#define	R_FIXLEN	0x00200		/* fixed length records */
+#define	R_MEMMAPPED	0x00400		/* memory mapped file. */
+#define	R_INMEM		0x00800		/* in-memory file */
+#define	R_MODIFIED	0x01000		/* modified file */
+#define	R_RDONLY	0x02000		/* read-only file */
+
+#define	B_DB_LOCK	0x04000		/* DB_LOCK specified. */
+#define	B_DB_SHMEM	0x08000		/* DB_SHMEM specified. */
+#define	B_DB_TXN	0x10000		/* DB_TXN specified. */
+	u_int32_t flags;
+} BTREE;
+
+void	db_btree __P((DB *, int));
+void	db_hash __P((DB *, int));
+void	dbt_dump __P((DBT *));
+void	dbt_print __P((DBT *));
+int	main __P((int, char *[]));
+int	usage __P((void));
+
+int
+main(argc, argv)
+	int argc;
+	char *argv[];
+{
+	extern char *optarg;
+	extern int optind;
+	DB *dbp;
+	DBT key, data;
+	int ch, pflag, rval;
+
+	pflag = 0;
+	while ((ch = getopt(argc, argv, "f:p")) != EOF)
+		switch (ch) {
+		case 'f':
+			if (freopen(optarg, "w", stdout) == NULL) {
+				fprintf(stderr, "db_dump185: %s: %s\n",
+				    optarg, strerror(errno));
+				return (EXIT_FAILURE);
+			}
+			break;
+		case 'p':
+			pflag = 1;
+			break;
+		case '?':
+		default:
+			return (usage());
+		}
+	argc -= optind;
+	argv += optind;
+
+	if (argc != 1)
+		return (usage());
+
+	if ((dbp = dbopen(argv[0], O_RDONLY, 0, DB_BTREE, NULL)) == NULL) {
+		if ((dbp =
+		    dbopen(argv[0], O_RDONLY, 0, DB_HASH, NULL)) == NULL) {
+			fprintf(stderr,
+			    "db_dump185: %s: %s\n", argv[0], strerror(errno));
+			return (EXIT_FAILURE);
+		}
+		db_hash(dbp, pflag);
+	} else
+		db_btree(dbp, pflag);
+
+	/*
+	 * !!!
+	 * DB 1.85 DBTs are a subset of DB 2.0 DBTs, so we just use the
+	 * new dump/print routines.
+	 */
+	if (pflag)
+		while (!(rval = dbp->seq(dbp, &key, &data, R_NEXT))) {
+			dbt_print(&key);
+			dbt_print(&data);
+		}
+	else
+		while (!(rval = dbp->seq(dbp, &key, &data, R_NEXT))) {
+			dbt_dump(&key);
+			dbt_dump(&data);
+		}
+
+	if (rval == -1) {
+		fprintf(stderr, "db_dump185: seq: %s\n", strerror(errno));
+		return (EXIT_FAILURE);
+	}
+	return (EXIT_SUCCESS);
+}
+
+/*
+ * db_hash --
+ *	Dump out hash header information.
+ */
+void
+db_hash(dbp, pflag)
+	DB *dbp;
+	int pflag;
+{
+	HTAB185 *hash185p;
+	HTAB186 *hash186p;
+
+	printf("format=%s\n", pflag ? "print" : "bytevalue");
+	printf("type=hash\n");
+
+	/* DB 1.85 was version 2, DB 1.86 was version 3. */
+	hash185p = dbp->internal;
+	if (hash185p->hdr.version > 2) {
+		hash186p = dbp->internal;
+		printf("h_ffactor=%lu\n", (u_long)hash186p->hdr.ffactor);
+		if (hash186p->hdr.lorder != 0)
+			printf("db_lorder=%lu\n", (u_long)hash186p->hdr.lorder);
+		printf("db_pagesize=%lu\n", (u_long)hash186p->hdr.bsize);
+	} else {
+		printf("h_ffactor=%lu\n", (u_long)hash185p->hdr.ffactor);
+		if (hash185p->hdr.lorder != 0)
+			printf("db_lorder=%lu\n", (u_long)hash185p->hdr.lorder);
+		printf("db_pagesize=%lu\n", (u_long)hash185p->hdr.bsize);
+	}
+	printf("HEADER=END\n");
+}
+
+/*
+ * db_btree --
+ *	Dump out btree header information.
+ */
+void
+db_btree(dbp, pflag)
+	DB *dbp;
+	int pflag;
+{
+	BTREE *btp;
+
+	btp = dbp->internal;
+
+	printf("format=%s\n", pflag ? "print" : "bytevalue");
+	printf("type=btree\n");
+#ifdef NOT_AVAILABLE_IN_185
+	printf("bt_minkey=%lu\n", (u_long)XXX);
+	printf("bt_maxkey=%lu\n", (u_long)XXX);
+#endif
+	if (btp->bt_lorder != 0)
+		printf("db_lorder=%lu\n", (u_long)btp->bt_lorder);
+	printf("db_pagesize=%lu\n", (u_long)btp->bt_psize);
+	if (!(btp->flags & B_NODUPS))
+		printf("duplicates=1\n");
+	printf("HEADER=END\n");
+}
+
+static char hex[] = "0123456789abcdef";
+
+/*
+ * dbt_dump --
+ *	Write out a key or data item using byte values.
+ */
+void
+dbt_dump(dbtp)
+	DBT *dbtp;
+{
+	size_t len;
+	u_int8_t *p;
+
+	for (len = dbtp->size, p = dbtp->data; len--; ++p)
+		(void)printf("%c%c",
+		    hex[(*p & 0xf0) >> 4], hex[*p & 0x0f]);
+	printf("\n");
+}
+
+/*
+ * dbt_print --
+ *	Write out a key or data item using printable characters.
+ */
+void
+dbt_print(dbtp)
+	DBT *dbtp;
+{
+	size_t len;
+	u_int8_t *p;
+
+	for (len = dbtp->size, p = dbtp->data; len--; ++p)
+		if (isprint((int)*p)) {
+			if (*p == '\\')
+				(void)printf("\\");
+			(void)printf("%c", *p);
+		} else
+			(void)printf("\\%c%c",
+			    hex[(*p & 0xf0) >> 4], hex[*p & 0x0f]);
+	printf("\n");
+}
+
+/*
+ * usage --
+ *	Display the usage message.
+ */
+int
+usage()
+{
+	(void)fprintf(stderr, "usage: db_dump185 [-p] [-f file] db_file\n");
+	return (EXIT_FAILURE);
+}
